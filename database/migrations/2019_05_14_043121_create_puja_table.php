<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePujaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('puja', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo')->nullable()->default(null);
            $table->double('total')->nullable()->default(null);
            $table->double('distancia')->nullable()->default(null);
            $table->double('personas')->nullable()->default(null);
            $table->timestamp('fecha')->useCurrent();
            $table->integer('state')->nullable()->default(1);

            $table->integer('cliente')->nullable()->default(null)->unsigned();
            $table->foreign('cliente')->references('id')->on('users')->onDelete('cascade');

            $table->integer('taxi')->nullable()->default(null)->unsigned();
            $table->foreign('taxi')->references('id')->on('users')->onDelete('cascade');

            $table->integer('pago_minimo')->nullable()->default(null)->unsigned();
            $table->foreign('pago_minimo')->references('id')->on('pago_minimo')->onDelete('cascade');

            $table->integer('recoger')->nullable()->default(null)->unsigned();
            $table->foreign('recoger')->references('id')->on('direcciones')->onDelete('cascade');

            $table->integer('llegar')->nullable()->default(null)->unsigned();
            $table->foreign('llegar')->references('id')->on('direcciones')->onDelete('cascade');

            $table->integer('tipo_pago')->nullable()->default(null)->unsigned();
            $table->foreign('tipo_pago')->references('id')->on('tipo_pago')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('puja');
    }
}

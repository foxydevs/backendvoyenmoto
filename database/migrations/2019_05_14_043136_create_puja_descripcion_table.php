<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePujaDescripcionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('puja_descripcion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo')->nullable()->default(null);
            $table->string('respuesta')->nullable()->default(null);
            $table->double('monto')->nullable()->default(null);
            $table->string('descripcion')->nullable()->default(null);
            $table->integer('state')->nullable()->default(1);

            $table->integer('puja')->nullable()->default(null)->unsigned();
            $table->foreign('puja')->references('id')->on('puja')->onDelete('cascade');

            $table->integer('cliente')->nullable()->default(null)->unsigned();
            $table->foreign('cliente')->references('id')->on('users')->onDelete('cascade');

            $table->integer('taxi')->nullable()->default(null)->unsigned();
            $table->foreign('taxi')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('puja_descripcion');
    }
}

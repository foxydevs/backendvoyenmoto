<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::resource('coordenadas', 'CoordenadasController');
Route::resource('direcciones', 'DireccionesController');
Route::resource('mensajeria', 'MensajeriaController');
Route::resource('pagominimo', 'PagominimoController');
Route::resource('puja', 'PujaController');
Route::resource('pujadescripcion', 'PujaDescripcionController');
Route::resource('taxis', 'TaxisController');
Route::resource('taxisfavs', 'TaxisFavsController');
Route::resource('tipodirecciones', 'TipoDireccionesController');
Route::resource('tipopago', 'TipoPagoController');
Route::resource('users', 'UsersController');
Route::resource('roles', 'RolesController');

Route::get('filter/{id}/coordenadas/{state}', "CoordenadasController@getThisByFilter");
Route::get('filter/{id}/direcciones/{state}', "DireccionesController@getThisByFilter");
Route::get('filter/{id}/mensajeria/{state}', "MensajeriaController@getThisByFilter");
Route::get('filter/{id}/pagominimo/{state}', "PagominimoController@getThisByFilter");
Route::get('filter/{id}/puja/{state}', "PujaController@getThisByFilter");
Route::get('filter/{id}/pujadescripcion/{state}', "PujaDescripcionController@getThisByFilter");
Route::get('filter/{id}/taxis/{state}', "TaxisController@getThisByFilter");
Route::get('filter/{id}/taxisfavs/{state}', "TaxisFavsController@getThisByFilter");
Route::get('filter/{id}/tipodirecciones/{state}', "TipoDireccionesController@getThisByFilter");
Route::get('filter/{id}/tipopago/{state}', "TipoPagoController@getThisByFilter");
Route::get('filter/{id}/users/{state}', "UsersController@getThisByFilter");
Route::get('filter/{id}/roles/{state}', "RolesController@getThisByFilter");

Route::get('rol/{id}/users', "Users@getUsersByRol");

Route::post('users/password/reset', 'UsersController@recoveryPassword');
Route::post('users/{id}/changepassword', "UsersController@changePassword");

Route::post('login', 'AuthenticateController@login');
Route::post('upload', 'AuthenticateController@uploadAvatar');

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mensajeria extends Model
{
    protected $table = 'mensajeria';

    public function receptor(){
        return $this->hasOne('App\Users','id','recibe');
    }

    public function emisor(){
        return $this->hasOne('App\Users','id','envia');
    }
}

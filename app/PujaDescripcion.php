<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PujaDescripcion extends Model
{
    protected $table = 'puja_descripcion';

    public function pujas(){
        return $this->hasOne('App\Puja','id','puja');
    }

    public function clientes(){
        return $this->hasOne('App\Users','id','cliente');
    }

    public function taxistas(){
        return $this->hasOne('App\Users','id','taxi');
    }
}

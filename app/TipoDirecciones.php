<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoDirecciones extends Model
{
    protected $table = 'tipo_direcciones';
}

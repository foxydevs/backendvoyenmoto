<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxisFavs extends Model
{
    protected $table = 'taxis_favs';

    public function usuarios(){
        return $this->hasOne('App\Users','id','usuario');
    }

    public function taxistas(){
        return $this->hasOne('App\Users','id','taxista');
    }
}

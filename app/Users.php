<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Users extends Authenticatable
{
    protected $table = 'users';

    protected $hidden = [
        'password'
    ];

    public function roles(){
        return $this->hasOne('App\Roles','id','rol');
    }

    public function direcciones(){
        return $this->hasMany('App\Direcciones','usuario','id');
    }

    public function taxis(){
        return $this->hasOne('App\Taxis','id','taxi');
    }

    public function misPujasPedidas(){
        return $this->hasMany('App\Puja','cliente','id');
    }

    public function misPujasAceptadas(){
        return $this->hasMany('App\Puja','taxi','id');
    }

    public function coordenadas(){
        return $this->hasMany('App\Coordenadas','usuario','id');
    }

    public function msgRecibidos(){
        return $this->hasMany('App\Mensajeria','recibe','id');
    }

    public function msgEnviados(){
        return $this->hasMany('App\Mensajeria','envia','id');
    }
}

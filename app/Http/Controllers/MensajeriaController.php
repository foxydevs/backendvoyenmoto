<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Mensajeria;
use Response;
use Validator;
class MensajeriaController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return Response::json(Mensajeria::all(), 200);
    }
    
    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state':{
                    $objectSee = Mensajeria::whereRaw('state=?',[$state])->with('receptor','emisor')->get();
                    break;
                }
                case 'envia':{
                    $objectSee = Mensajeria::whereRaw('envia=?',[$state])->with('receptor','emisor')->get();
                    break;
                }
                case 'recibe':{
                    $objectSee = Mensajeria::whereRaw('recibe=?',[$state])->with('receptor','emisor')->get();
                    break;
                }
                default:{
                    $objectSee = Mensajeria::whereRaw('state=?',[$state])->with('receptor','emisor')->get();
                    break;
                }
    
            }
        }else{
            $objectSee = Mensajeria::with('receptor','emisor')->get();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'recibe'          => 'required',
            'envia'          => 'required',
            'mensaje'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new Mensajeria();
                $newObject->titulo            = $request->get('titulo');
                $newObject->mensaje            = $request->get('mensaje');
                $newObject->foto            = $request->get('foto');
                $newObject->link            = $request->get('link');
                $newObject->adjunto            = $request->get('adjunto');
                $newObject->state            = $request->get('state');
                $newObject->envia            = $request->get('envia');
                $newObject->recibe            = $request->get('recibe');
                $newObject->save();
                return Response::json($newObject, 200);
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $objectSee = Mensajeria::find($id);
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $objectUpdate = Mensajeria::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->titulo = $request->get('titulo', $objectUpdate->titulo);
                $objectUpdate->mensaje = $request->get('mensaje', $objectUpdate->mensaje);
                $objectUpdate->foto = $request->get('foto', $objectUpdate->foto);
                $objectUpdate->link = $request->get('link', $objectUpdate->link);
                $objectUpdate->adjunto = $request->get('adjunto', $objectUpdate->adjunto);
                $objectUpdate->state = $request->get('state', $objectUpdate->state);
                $objectUpdate->envia = $request->get('envia', $objectUpdate->envia);
                $objectUpdate->recibe = $request->get('recibe', $objectUpdate->recibe);
    
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $objectDelete = Mensajeria::find($id);
        if ($objectDelete) {
            try {
                Mensajeria::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Puja;
use Response;
use Validator;
class PujaController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return Response::json(Puja::all(), 200);
    }
    
    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state':{
                    $objectSee = Puja::whereRaw('state=?',[$state])->with('clientes','taxistas','pagoMinimo','recogerEn','llegarA','tipoPago')->get();
                    break;
                }
                case 'cliente':{
                    $objectSee = Puja::whereRaw('cliente=?',[$state])->with('clientes','taxistas','pagoMinimo','recogerEn','llegarA','tipoPago')->get();
                    break;
                }
                case 'taxi':{
                    $objectSee = Puja::whereRaw('taxi=?',[$state])->with('clientes','taxistas','pagoMinimo','recogerEn','llegarA','tipoPago')->get();
                    break;
                }
                case 'pago_minimo':{
                    $objectSee = Puja::whereRaw('pago_minimo=?',[$state])->with('clientes','taxistas','pagoMinimo','recogerEn','llegarA','tipoPago')->get();
                    break;
                }
                case 'recoger':{
                    $objectSee = Puja::whereRaw('recoger=?',[$state])->with('clientes','taxistas','pagoMinimo','recogerEn','llegarA','tipoPago')->get();
                    break;
                }
                case 'llegar':{
                    $objectSee = Puja::whereRaw('llegar=?',[$state])->with('clientes','taxistas','pagoMinimo','recogerEn','llegarA','tipoPago')->get();
                    break;
                }
                case 'tipo_pago':{
                    $objectSee = Puja::whereRaw('tipo_pago=?',[$state])->with('clientes','taxistas','pagoMinimo','recogerEn','llegarA','tipoPago')->get();
                    break;
                }
                default:{
                    $objectSee = Puja::whereRaw('state=?',[$state])->with('clientes','taxistas','pagoMinimo','recogerEn','llegarA','tipoPago')->get();
                    break;
                }
    
            }
        }else{
            $objectSee = Puja::with('clientes','taxistas','pagoMinimo','recogerEn','llegarA','tipoPago')->get();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'total'          => 'required',
            'cliente'          => 'required',
            'recoger'          => 'required',
            'llegar'          => 'required'
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new Puja();
                $newObject->titulo            = $request->get('titulo');
                $newObject->total            = $request->get('total');
                $newObject->distancia            = $request->get('distancia');
                $newObject->personas            = $request->get('personas');
                $newObject->fecha            = $request->get('fecha');
                $newObject->state            = $request->get('state');
                $newObject->cliente            = $request->get('cliente');
                $newObject->taxi            = $request->get('taxi');
                $newObject->pago_minimo            = $request->get('pago_minimo');
                $newObject->recoger            = $request->get('recoger');
                $newObject->llegar            = $request->get('llegar');
                $newObject->tipo_pago            = $request->get('tipo_pago');
                $newObject->save();
                return Response::json($newObject, 200);
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $objectSee = Puja::with('clientes','taxistas','pagoMinimo','recogerEn','llegarA','tipoPago')->find($id);
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $objectUpdate = Puja::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->titulo = $request->get('titulo', $objectUpdate->titulo);
                $objectUpdate->total = $request->get('total', $objectUpdate->total);
                $objectUpdate->distancia = $request->get('distancia', $objectUpdate->distancia);
                $objectUpdate->personas = $request->get('personas', $objectUpdate->personas);
                $objectUpdate->fecha = $request->get('fecha', $objectUpdate->fecha);
                $objectUpdate->state = $request->get('state', $objectUpdate->state);
                $objectUpdate->cliente = $request->get('cliente', $objectUpdate->cliente);
                $objectUpdate->taxi = $request->get('taxi', $objectUpdate->taxi);
                $objectUpdate->pago_minimo = $request->get('pago_minimo', $objectUpdate->pago_minimo);
                $objectUpdate->recoger = $request->get('recoger', $objectUpdate->recoger);
                $objectUpdate->llegar = $request->get('llegar', $objectUpdate->llegar);
                $objectUpdate->tipo_pago = $request->get('tipo_pago', $objectUpdate->tipo_pago);
    
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $objectDelete = Puja::find($id);
        if ($objectDelete) {
            try {
                Puja::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\PujaDescripcion;
use Response;
use Validator;
class PujaDescripcionController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return Response::json(PujaDescripcion::all(), 200);
    }
    
    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state':{
                    $objectSee = PujaDescripcion::whereRaw('state=?',[$state])->with('clientes','taxistas','pujas')->get();
                    break;
                }
                case 'puja':{
                    $objectSee = PujaDescripcion::whereRaw('puja=?',[$state])->with('clientes','taxistas','pujas')->get();
                    break;
                }
                case 'cliente':{
                    $objectSee = PujaDescripcion::whereRaw('cliente=?',[$state])->with('clientes','taxistas','pujas')->get();
                    break;
                }
                case 'taxi':{
                    $objectSee = PujaDescripcion::whereRaw('taxi=?',[$state])->with('clientes','taxistas','pujas')->get();
                    break;
                }
                default:{
                    $objectSee = PujaDescripcion::whereRaw('state=?',[$state])->with('clientes','taxistas','pujas')->get();
                    break;
                }
    
            }
        }else{
            $objectSee = PujaDescripcion::with('clientes','taxistas','pujas')->get();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cliente'          => 'required',
            'taxi'          => 'required',
            'puja'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new PujaDescripcion();
                $newObject->titulo            = $request->get('titulo');
                $newObject->respuesta            = $request->get('respuesta');
                $newObject->monto            = $request->get('monto');
                $newObject->descripcion            = $request->get('descripcion');
                $newObject->state            = $request->get('state');
                $newObject->puja            = $request->get('puja');
                $newObject->cliente            = $request->get('cliente');
                $newObject->taxi            = $request->get('taxi');
                $newObject->save();
                return Response::json($newObject, 200);
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $objectSee = PujaDescripcion::find($id);
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $objectUpdate = PujaDescripcion::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->titulo = $request->get('titulo', $objectUpdate->titulo);
                $objectUpdate->respuesta = $request->get('respuesta', $objectUpdate->respuesta);
                $objectUpdate->monto = $request->get('monto', $objectUpdate->monto);
                $objectUpdate->descripcion = $request->get('descripcion', $objectUpdate->descripcion);
                $objectUpdate->state = $request->get('state', $objectUpdate->state);
                $objectUpdate->puja = $request->get('puja', $objectUpdate->puja);
                $objectUpdate->cliente = $request->get('cliente', $objectUpdate->cliente);
                $objectUpdate->taxi = $request->get('taxi', $objectUpdate->taxi);
    
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $objectDelete = PujaDescripcion::find($id);
        if ($objectDelete) {
            try {
                PujaDescripcion::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}

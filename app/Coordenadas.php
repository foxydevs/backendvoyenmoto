<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coordenadas extends Model
{
    protected $table = 'coordenadas';

    public function usuarios(){
        return $this->hasOne('App\Users','id','usuario');
    }
}

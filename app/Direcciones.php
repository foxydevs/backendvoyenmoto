<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Direcciones extends Model
{
    protected $table = 'direcciones';

    public function usuarios(){
        return $this->hasOne('App\Users','id','usuario');
    }

    public function tipos(){
        return $this->hasOne('App\TipoDirecciones','id','tipo');
    }
}

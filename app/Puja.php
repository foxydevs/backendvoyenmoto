<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Puja extends Model
{
    protected $table = 'puja';

    public function clientes(){
        return $this->hasOne('App\Users','id','cliente');
    }

    public function taxistas(){
        return $this->hasOne('App\Users','id','taxi');
    }

    public function pagoMinimo(){
        return $this->hasOne('App\PagoMinimo','id','pago_minimo');
    }

    public function recogerEn(){
        return $this->hasOne('App\Direcciones','id','recoger');
    }

    public function llegarA(){
        return $this->hasOne('App\Direcciones','id','llegar');
    }

    public function tipoPago(){
        return $this->hasOne('App\TipoPago','id','tipo_pago');
    }
}
